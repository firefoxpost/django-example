from django.conf.urls import url

from .views import GetToken, ListEvents, DetailEvents, CreateRegistration

app_name = 'rest_api'

urlpatterns = [
    url(r'^auth/', GetToken.as_view()),
    url(r'^events/$', ListEvents.as_view(), name='events-list'),
    url(r'^events/(?P<pk>\d+)/$', DetailEvents.as_view()),
    url(r'^events/register/$', CreateRegistration.as_view()),   
]

