from rest_framework import serializers
from events.models import Event, Registration
from django.contrib.auth import get_user_model


User = get_user_model()


class EventsSerializer(serializers.ModelSerializer):
    users = serializers.SerializerMethodField()
    event_datetime = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = ('pk', 'name', 'text', 'event_datetime', 'users')

    def get_users(self, obj, *args, **kwargs):
        registrations = Registration.objects.filter(event=obj)
        return [r.user.username for r in registrations]

    def get_event_datetime(self, obj, *args, **kwargs):
        return obj.event_datetime.strftime('%d-%m-%Y %H:%m')

class CreateRegistrationSerializer(serializers.Serializer):
    event = serializers.IntegerField(required=True)

