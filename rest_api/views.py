from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK,
                                   HTTP_201_CREATED,
                                   HTTP_404_NOT_FOUND,
                                   HTTP_400_BAD_REQUEST,
                                   HTTP_401_UNAUTHORIZED,
                                   HTTP_403_FORBIDDEN,
                                   HTTP_503_SERVICE_UNAVAILABLE)
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.generics import ListAPIView, RetrieveAPIView

from django.contrib.auth import get_user_model

from events.models import Event, Registration
from .serializers import EventsSerializer, CreateRegistrationSerializer



class GetToken(APIView):
    """
    ### Метод получения токена авторизации
    **Метод используется без стандартного заголовка авторизации, вместо него используется Basic Auth заголовок.**

    | Параметр             | Значение                                    |
    |----------------------|:--------------------------------------------|
    | URL                  | /auth/                                      |
    | Метод                | GET                                         |
    | Заголовок акторизации| Authorization: Basic [логин:пароль в base64]|


    Поля запроса:
    нет

    Поля ответа:

    | Параметр             | Значение                                    |
    |----------------------|:--------------------------------------------|
    | token                | Значение токена в случае успеха авторизации |
    | detail               | Описание ошибки                             |

    """

    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user_model = get_user_model()
        user = user_model.objects.get(pk=self.request.user.pk)
        token = Token.objects.get_or_create(user=user)[0].key
        return Response({"token": token})


class ListEvents(ListAPIView):
    """
    ### Список мероприятий

    | Параметр             | Значение                                 |
    |----------------------|:-----------------------------------------|
    | URL                  | /events/                                 |
    | Метод                | GET                                      |

    Поля ответа:

    | Поле                 | Описание                                               |
    |----------------------|:-------------------------------------------------------|
    | pk                   | Идентификатор                                          |
    | name                 | Название                                               |
    | text                 | Описание                                               |
    | event_datetime       | Дата и время мероприятия                               |
    | users                | Список зарегистрированных на мероприятие пользователей |

    """

    queryset = Event.objects.all()
    serializer_class = EventsSerializer


class DetailEvents(RetrieveAPIView):
    """
    ### Мероприятие детально

    | Параметр             | Значение                                 |
    |----------------------|:-----------------------------------------|
    | URL                  | /events/<pk>/                            |
    | Метод                | GET                                      |

    Поля ответа:

    | Поле                 | Описание                                               |
    |----------------------|:-------------------------------------------------------|
    | pk                   | Идентификатор                                          |
    | name                 | Название                                               |
    | text                 | Описание                                               |
    | event_datetime       | Дата и время мероприятия                               |
    | users                | Список зарегистрированных на мероприятие пользователей |

    """

    queryset = Event.objects.all()
    serializer_class = EventsSerializer


class CreateRegistration(APIView):
    """
    ### Регистрация на мероприятие

    | Параметр             | Значение                                 |
    |----------------------|:-----------------------------------------|
    | URL                  | /events/register/                        |
    | Метод                | POST                                     |
    
    | Параметр       | Тип данных     | Описание                    | Обязательность    |
    |----------------|:---------------|-----------------------------|-------------------|
    | event          | Int            | Идентификатор мероприятия   | Обязательно       |
    
    Поля ответа: нет

    """

    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        try:
            serializer = CreateRegistrationSerializer(data=request.data)
            if serializer.is_valid():
                event_pk = request.data.get("event")

                try:
                    target_event = Event.objects.get(pk=event_pk)
                except Event.DoesNotExist:
                    return Response({"detail": "Мероприятие с запрашиваемым ID не найдено"}, status=HTTP_404_NOT_FOUND)
                
                registration = Registration.objects.create(user=self.request.user, event=target_event)
                
                return Response(status=HTTP_201_CREATED)

            raise Exception(serializer.errors)
        except Exception as e:
            return Response({"detail": str(e)}, status=HTTP_400_BAD_REQUEST)

