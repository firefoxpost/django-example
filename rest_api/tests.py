from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from events.models import Event, Registration
from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token


User = get_user_model()


class ListEventsTests(APITestCase):
    def setUp(self):
        Event.objects.create(name="Test event 1", text="Test text to event 1", event_datetime="2019-12-08T20:00:00+00:00")
        Event.objects.create(name="Test event 2", text="Test text to event 2", event_datetime="2019-12-14T13:00:00+00:00")
        Event.objects.create(name="Test event 3", text="Test text to event 3", event_datetime="2019-12-18T09:00:00+00:00")

    def test_get_list_events(self):
        """
        Check events list API method
        """
        url = 'http://localhost/api/events/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0]["name"], 'Test event 1')


class DetailEventsTests(APITestCase):
    def setUp(self):
        Event.objects.create(name="Test event 1", text="Test text to event 1", event_datetime="2019-12-08T20:00:00+00:00")
        Event.objects.create(name="Test event 2", text="Test text to event 2", event_datetime="2019-12-14T13:00:00+00:00")
        Event.objects.create(name="Test event 3", text="Test text to event 3", event_datetime="2019-12-18T09:00:00+00:00")
    
    def test_get_detail_event(self):
        """
        Check events get API method
        """
        event_two = Event.objects.get(name="Test event 2")
        url = 'http://localhost/api/events/' + str(event_two.pk) + '/'
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['pk'], event_two.pk)
        self.assertEqual(response.data["name"], 'Test event 2')


class CreateRegistrationTests(APITestCase):
    def setUp(self):
        Event.objects.create(name="Test event 1", text="Test text to event 1", event_datetime="2019-12-08T20:00:00+00:00")
        user, created = User.objects.get_or_create(username="testing_user")
        Token.objects.create(user=user)
        
    
    def test_registration(self):
        """
        Check registration create API method
        """
        event_one = Event.objects.get(name="Test event 1")
        token = Token.objects.get(user__username='testing_user')
    
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        req_data = {"event": event_one.pk}
        url = 'http://localhost/api/events/register/'
        response = self.client.post(url, data=req_data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
    
