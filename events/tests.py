from django.test import TestCase
from .models import Event, Registration
from django.contrib.auth import get_user_model


User = get_user_model()


class EventTestCase(TestCase):
    def setUp(self):
        Event.objects.create(name="Test event 1", text="Test text to event 1", event_datetime="2019-12-08T20:00:00+00:00")
    
    def test_event_text(self):
        """Text of events is correct"""
        event_one = Event.objects.get(name="Test event 1")
        self.assertEqual(event_one.text, 'Test text to event 1')
        
    def test_event_date(self):
        """If date of event is correct"""
        event_one = Event.objects.get(name="Test event 1")
        date_str = event_one.event_datetime.strftime('%d-%m-%Y')
        self.assertEqual(date_str, '08-12-2019')

    def test_event_time(self):
        """If time of event is correct"""
        event_one = Event.objects.get(name="Test event 1")
        time_str = event_one.event_datetime.strftime('%H:%M')
        self.assertEqual(time_str, '20:00')


class RegistrationCase(TestCase):
    def setUp(self):
        event_one, created = Event.objects.get_or_create(name="Test event 1", text="Test text to event 1", event_datetime="2019-12-08T20:00:00+00:00")
        user_one, created = User.objects.get_or_create(username="testing_user")    
        Registration.objects.create(user=user_one, event=event_one)
    
    def test_registration(self):
        """If registration is correct"""
        user_one = User.objects.get(username="testing_user")
        event = Event.objects.get(name='Test event 1')
        
        self.assertEqual(Registration.objects.filter(user=user_one, event=event).count(), 1)

