from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from django.utils import timezone


User = get_user_model()


class Event(models.Model):
    name = models.CharField(verbose_name=_('Название'), max_length=100, null=False, blank=False)
    text = models.TextField(verbose_name=_('Описание'), max_length=3000, null=False, blank=False)
    event_datetime = models.DateTimeField(null=False, blank=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Мероприятие')
        verbose_name_plural = "Мероприятия"


class Registration(models.Model):
    user = models.ForeignKey(User, verbose_name=_('Пользователь'), on_delete=models.CASCADE)
    event = models.ForeignKey(Event, verbose_name=_('Мероприятие'), on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Регистрация на мероприятие')
        verbose_name_plural = "Регистрации на мероприятия"

        constraints = [
            models.UniqueConstraint(fields=['user', 'event'], name="user-event-registration")
        ]
        
        ordering = ('user',)

    def __str__(self):
        return self.user.username

