from django.contrib import admin
from django.contrib.auth import get_user_model

from .models import Event, Registration


User = get_user_model()


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'text', 'event_datetime', 'pk')
    

@admin.register(Registration)
class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('user', 'view_event', 'pk')
    
    def view_event(self, obj):
        return obj.event.name

