# django-example

## Description

Example of REST-api service with Django and Django REST framework


## Configuration

Create env files .django and .postgres with example files into ".envs/.local" of root project folder


## Requirements

All requirements are in the file requirements.txt. "pip install -r requirements.txt" runs automatically while docker build process 


## Startup

Startup the application using docker-compose command, with docker-compose.yml. 
In first startup, create django admin user with command "sudo docker-compose -f docker-compose.yml run --rm django python3 manage.py createsuperuser"


## REST-api docs

REST-api documentation file is API.md


## Testing

Unit-tests are located in events/tests.py and rest_api/tests.py. Tests run automatically while docker is going up

